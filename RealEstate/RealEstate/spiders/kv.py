import scrapy


class KVSaleSpider(scrapy.Spider):
    name = 'kv-sell'

    def start_requests(self):
        urls = [
            'https://www.kv.ee/kinnisvara/korterid',
            'https://www.kv.ee/kinnisvara/majad',
            'https://www.kv.ee/kinnisvara/maa',
            'https://www.kv.ee/kinnisvara/kontor',
            'https://www.kv.ee/kinnisvara/uusarendused',
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse_detail(self, response):
        yield {
            'title': response.xpath("//h1[@class='title']/text()").extract_first(),
            'address': response.xpath("//div[@class='inner']/div[@class='text-small']/text()").extract_first()[27:],
            'longitude': response.xpath("//a[@class='gtm-object-map']/@href").extract_first()[48:].split(',')[0],
            'latitude': response.xpath("//a[@class='gtm-object-map']/@href").extract_first()[48:].split(',')[1],
            'price': response.xpath("//div[@class='object-price']/strong/text()").extract_first(),
            'info_table': response.xpath("//div[@class='object-article-details']").extract(),
            'info_body': response.xpath("//div[@class='object-article-body']/p/text()").extract(),
        }

    def parse(self, response):
        links = response.xpath('//a[@class="object-title-a text-truncate"]/@href').extract()

        for link in links:
            yield scrapy.Request(url=link, callback=self.parse_detail)

        next_page = response.xpath("//li[@class='item']/a[@title='Järgmine']/@href").extract_first()

        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(url=next_page, callback=self.parse)
